package com.asggo.g21.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/4/28 14:40
 */
@Configuration
@MapperScan("com.asggo.g21.biz.mapper")
public class MyBatisPlusConfig {

}
