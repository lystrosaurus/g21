package com.asggo.g21.config;

import com.asggo.g21.listener.SseTopicListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/3/27 18:05
 */
@Configuration
public class SseConfig {

  @Bean
  public SseTopicListener sseTopicListener() {
    return new SseTopicListener();
  }
}
