package com.asggo.g21.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/2/19 15:44
 */
@Configuration
public class Bucket4jConfig {
  @Bean
  public Config config() {
    Config config = new Config();
    config.setInstanceName("hazelcast-instance")
        .addMapConfig(new MapConfig().setName("hazelcast-map"));
    return config;
  }
}
