package com.asggo.g21.listener;

import cn.hutool.core.collection.CollUtil;
import com.asggo.g21.holder.WebSocketSessionHolder;
import com.asggo.g21.utils.WebSocketUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.Ordered;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 16:22
 */
@Slf4j
public class WebSocketTopicListener implements ApplicationRunner, Ordered {

  @Override
  public void run(ApplicationArguments args) {
    WebSocketUtils.subscribeMessage(message -> {
      log.info("WebSocket主题订阅收到消息session keys={} message={}", message.getSessionKeys(), message.getMessage());
      // 如果key不为空就按照key发消息 如果为空就群发
      if (CollUtil.isNotEmpty(message.getSessionKeys())) {
        message.getSessionKeys().forEach(key -> {
          if (WebSocketSessionHolder.existSession(key)) {
            WebSocketUtils.sendMessage(key, message.getMessage());
          }
        });
      } else {
        WebSocketSessionHolder.getAllSessionsKey().forEach(key
            -> WebSocketUtils.sendMessage(key, message.getMessage()));
      }
    });
    log.info("初始化WebSocket主题订阅监听器成功");
  }

  @Override
  public int getOrder() {
    return -1;
  }
}
