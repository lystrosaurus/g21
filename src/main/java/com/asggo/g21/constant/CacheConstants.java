package com.asggo.g21.constant;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/20 12:51
 */
public class CacheConstants {
  private CacheConstants() {}

  /**
   * websocket 主题 key.
   */
  public static final String WEB_SOCKET_TOPIC = "gs:ws";

  /**
   * sse 主题 key.
   */
  public static final String SSE_EMITTER_TOPIC = "gs:sse";
}
