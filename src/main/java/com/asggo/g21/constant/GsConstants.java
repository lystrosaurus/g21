package com.asggo.g21.constant;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 16:50
 */
public class GsConstants {

  private GsConstants() {
  }

  public static final String USER_ID = "USER_ID";
}
