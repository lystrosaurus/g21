package com.asggo.g21.payload.fs;

import lombok.Data;

@Data
public class TextContent implements Content {

  /**
   * 消息内容文本.
   */
  private String text;
}
