package com.asggo.g21.payload.fs;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * 飞书消息
 * @author eric 2023/3/29 15:45
 */
@Data
public class Message {
  /**
   * 消息类型 --> 后续可以使用枚举扩展.
   */
  @JsonProperty("msg_type")
  private String msgType = "text";

  /**
   * 消息内容.
   */
  private Content content;
}
