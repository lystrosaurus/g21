package com.asggo.g21.payload.lann;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/2/22 15:29
 */
@Getter
@Setter
@EqualsAndHashCode
public class Dk {

  @ExcelProperty("姓名")
  private String name;
  @ExcelProperty("中文名")
  private String chName;
  @ExcelProperty("工号")
  private String empNo;
  @ExcelProperty("入职日期(按日期)")
  private String data;
  @ExcelProperty("四级组织")
  private String org;
  @ExcelProperty("四月")
  private String april;
  @ExcelProperty("五月")
  private String may;
  @ExcelProperty("六月")
  private String june;
  @ExcelProperty("七月")
  private String july;
  @ExcelProperty("八月")
  private String august;
  @ExcelProperty("九月")
  private String september;
  @ExcelProperty("十月")
  private String october;
  @ExcelProperty("十一月")
  private String november;
  @ExcelProperty("十二月")
  private String december;
}
