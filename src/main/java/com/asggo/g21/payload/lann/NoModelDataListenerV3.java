package com.asggo.g21.payload.lann;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.util.ListUtils;
import com.anwen.mongo.mapper.MongoPlusMapMapper;
import com.asggo.g21.utils.RedisUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/6/4 15:38
 */
@Slf4j
public class NoModelDataListenerV3 extends AnalysisEventListener<Map<Integer, Object>> {

  /**
   * 每隔5条存储数据库，实际使用中可以100条，然后清理list ，方便内存回收
   */
  private static final int BATCH_COUNT = 50000;
  private final String sheetName;
  private final List<String> realHead = new ArrayList<>();
  private final List<String> head = new ArrayList<>();
  private List<Map<String, Object>> cachedDataList = ListUtils.newArrayListWithExpectedSize(
      BATCH_COUNT);

  public NoModelDataListenerV3(String sheetName) {
    this.sheetName = sheetName;
    realHead.add("月份");
    realHead.add("品牌名");
  }

  @Override
  public void invokeHeadMap(Map<Integer, String> data, AnalysisContext context) {
    log.info("sheet ->{}", sheetName);

    head.addAll(data.values());

    String headKey = "dp:" + sheetName + ":head";
    // 已经存在的表头
    final List<String> headList = RedisUtils.getCacheList(headKey);
    merge(headList, head);

    RedisUtils.setCacheList(headKey, headList);
  }

  private void merge(List<String> headList, List<String> head) {
    for (String s : head) {
      if (headList.contains(s)) {
        continue;
      }
      headList.add(s);
    }
  }


  @Override
  public void invoke(Map<Integer, Object> data, AnalysisContext context) {
    final Map<String, Object> collected = data.entrySet().stream()
        .filter(k -> k.getValue() != null
            && realHead.contains(head.get(k.getKey())))
        .collect(Collectors.toMap(k -> head.get(k.getKey()), Entry::getValue, (o, n) -> n,
            LinkedHashMap::new));
    final Object v = collected.get("品牌名");
    final String string = v.toString();
    String tags = "";
    if (string.contains("泰式") || string.contains("thai")) {
      tags += "泰式";
    }
    if (string.contains("中式")) {
      if (tags.isBlank()) {
        tags = "中式";
      } else {
        tags += ",中式";
      }
    }
    if (string.contains("SPA") || string.contains("spa")) {
      if (tags.isBlank()) {
        tags = "SPA";
      } else {
        tags += ",SPA";
      }
    }

    if (tags.isBlank()) {
      tags += "其他";
    }

    collected.put("品牌标签", tags);

    cachedDataList.add(collected);

    if (cachedDataList.size() >= BATCH_COUNT) {
      saveData();
      cachedDataList = ListUtils.newArrayListWithExpectedSize(BATCH_COUNT);
    }
  }

  @Override
  public void doAfterAllAnalysed(AnalysisContext context) {
    saveData();
    log.info("所有数据解析完成！");
  }

  /**
   * 加上存储数据库
   */
  private void saveData() {
    log.info("{}条数据，开始存储数据库！", cachedDataList.size());
    String nameKey = "DP:BRAND_NAME";
    String dataKey = "DP:BRAND:";
    for (Map<String, Object> dataMap : cachedDataList) {
      final Object name = dataMap.get("品牌名");
      // 如果名称存在, 则跳过,否则添加的数据表,并记录月份
      if (RedisUtils.cacheSetContains(nameKey, name)) {
        continue;
      }
      log.info("第一次记录名称 ->{}", dataMap);
      RedisUtils.setCacheSet(nameKey, name);
      RedisUtils.setCacheMap(dataKey + name, dataMap);
    }

    log.info("存储数据库成功！");
  }
}
