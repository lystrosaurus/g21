package com.asggo.g21.payload.lann;

import com.alibaba.excel.annotation.ExcelProperty;
import java.math.BigDecimal;
import java.util.Date;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/1/9 10:44
 */
@Getter
@Setter
@EqualsAndHashCode
public class DemoData {
  @ExcelProperty("序号")
  private Integer id;
  @ExcelProperty("订单编号")
  private Integer orderId;
  @ExcelProperty("GUID")
  private String guid;
  @ExcelProperty("产品类型")
  private Integer prodType;
  @ExcelProperty("产品名称")
  private String prodName;
  @ExcelProperty("产品价格")
  private BigDecimal prodAmt;
  @ExcelProperty("理疗师ID")
  private Integer empId;
  @ExcelProperty("创建时间")
  private Date createDate;
}
