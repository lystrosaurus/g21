package com.asggo.g21.ex;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/4/15 10:07
 */
public class G21Exception extends RuntimeException {

  public G21Exception() {
    super();
  }

  public G21Exception(Exception e) {
    super(e);
  }
}
