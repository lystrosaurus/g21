package com.asggo.g21.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import com.asggo.g21.annotation.SignatureValidation;
import com.asggo.g21.dto.UserDTO;
import com.asggo.g21.stomp.Greeting;
import com.asggo.g21.stomp.HelloMessage;
import com.asggo.g21.utils.IpUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.util.HtmlUtils;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 14:04
 */
@Controller
public class GreetingController {

  @MessageMapping("/hello")
  @SendTo("/topic/greetings")
  public Greeting greeting(HelloMessage message) throws InterruptedException {
    Thread.sleep(1000); // simulated delay
    return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
  }

  @GetMapping("/")
  @SaIgnore
  public ResponseEntity<String> home() {
    return ResponseEntity.ok("您好~,您的IP是 " + IpUtils.getClientIpAddressIfServletRequestExist()
        + " 位置信息:" + IpUtils.getRegion(IpUtils.getClientIpAddressIfServletRequestExist()));
  }

  @PostMapping("/test/body")
  @SaIgnore
  @SignatureValidation
  public ResponseEntity<String> test(@RequestBody UserDTO user) {
    return ResponseEntity.ok("test" + user.getEmail());
  }
}
