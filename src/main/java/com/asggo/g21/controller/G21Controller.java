package com.asggo.g21.controller;

import com.asggo.g21.domain.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/20 11:50
 */
@RestController
@RequestMapping("/g21")
@Slf4j
public class G21Controller {

  @RequestMapping("virtualThread")
  public ResponseEntity<R<String>> virtualThread() {
    Thread.startVirtualThread(() -> {
      for (int i = 8; i < 10000; ++i) {
        log.info("i ->{}", i);

        try {
          Thread.sleep(100);
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
        }
      }
    });

    return ResponseEntity.ok(R.ok());
  }
}
