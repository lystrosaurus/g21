package com.asggo.g21.controller;

import cn.dev33.satoken.session.SaSession;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.asggo.g21.dto.LoginDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 18:53
 */
@RestController
@RequestMapping("/user")
public class UserController {

  @PostMapping("/sign/in")
  public ResponseEntity<SaTokenInfo> signIn(@RequestBody LoginDTO user01) {
    if (user01 != null && StringUtils.hasLength(user01.getUsername())
        && StringUtils.hasLength(user01.getPassword())) {

      StpUtil.login(user01.getUsername().hashCode());

      StpUtil.getSession().set(SaSession.USER, user01);

      return ResponseEntity.ok(StpUtil.getTokenInfo());
    }

    return ResponseEntity.badRequest().build();
  }

  @GetMapping("/sign/out")
  public ResponseEntity<Void> signOut() {
    StpUtil.logout();

    return ResponseEntity.ok().build();
  }
}
