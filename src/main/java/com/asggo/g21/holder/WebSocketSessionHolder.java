package com.asggo.g21.holder;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.web.socket.WebSocketSession;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 16:31
 */
public class WebSocketSessionHolder {

  private WebSocketSessionHolder() {
  }

  private static final Map<Long, WebSocketSession> webSocketSessionMaps = new ConcurrentHashMap<>();

  public static void addSession(Long sessionKey, WebSocketSession session) {
    webSocketSessionMaps.put(sessionKey, session);
  }

  public static WebSocketSession removeSession(Long sessionKey) {
    return webSocketSessionMaps.remove(sessionKey);
  }

  public static WebSocketSession getSession(Long sessionKey) {
    return webSocketSessionMaps.get(sessionKey);
  }

  public static Set<Long> getAllSessionsKey() {
    return webSocketSessionMaps.keySet();
  }

  public static boolean existSession(Long sessionKey) {
    return webSocketSessionMaps.containsKey(sessionKey);
  }
}
