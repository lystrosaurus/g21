package com.asggo.g21.holder;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/3/27 17:31
 */
public class SseEmitterHolder {

  private SseEmitterHolder() {
  }

  private static final Map<String, SseEmitter> sseEmitterMap = new ConcurrentHashMap<>();

  public static void addSseEmitter(String sessionKey, SseEmitter sseEmitter) {
    sseEmitterMap.put(sessionKey, sseEmitter);
  }

  public static void removeSseEmitter(String sessionKey) {
    sseEmitterMap.remove(sessionKey);
  }

  public static SseEmitter getSseEmitter(String sessionKey) {
    return sseEmitterMap.get(sessionKey);
  }

  public static Set<String> getAllSessionKeys() {
    return sseEmitterMap.keySet();
  }

  public static boolean existSession(String sessionKey) {
    return sseEmitterMap.containsKey(sessionKey);
  }
}
