package com.asggo.g21.utils.compare;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2022/12/2 10:42
 */
@Data
public class User {
    @Compare("姓名")
    private String name;

    @Compare("年龄")
    private Integer age;

    @Compare(value = "性别", isMeta = true)
    private Integer sex;

    private String address;
}
