package com.asggo.g21.utils.compare;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2022/12/2 10:22
 */
@Data
public class CompareNode {
    /**
     * 字段
     */
    private String fieldKey;

    /**
     * 字段值
     */
    private Object fieldValue;

    /**
     * 字段名称
     */
    private String fieldName;
}
