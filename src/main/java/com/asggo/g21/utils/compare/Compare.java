package com.asggo.g21.utils.compare;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2022/12/2 10:21
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Compare {
    /**
     * 字段中文名称.
     *
     * @return 字段中文名称.
     */
    String value();

    /**
     * 是否元数据 默认 false.
     *
     * @return true/false.
     */
    boolean isMeta() default false;
}
