package com.asggo.g21.utils.compare;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/4/15 10:10
 */
@Data
public class DiffNode {

  private String fieldKey;
  private String fieldName;
  private Object sourceFieldValue;
  private Object targetFieldValue;
}
