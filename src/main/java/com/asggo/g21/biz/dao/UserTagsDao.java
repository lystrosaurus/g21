package com.asggo.g21.biz.dao;

import com.asggo.g21.biz.domain.UserTags;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
* @author eric
*/
public interface UserTagsDao extends IService<UserTags> {

  List<UserTags> getByCategory(String category);

  UserTags getByUid(int uid);
}
