package com.asggo.g21.biz.dao.impl;

import com.asggo.g21.biz.dao.UserTagsDao;
import com.asggo.g21.biz.domain.UserTags;
import com.asggo.g21.biz.mapper.UserTagsMapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 * @author eric
 */
@Service
public class UserTagsDaoImpl extends ServiceImpl<UserTagsMapper, UserTags>
    implements UserTagsDao {

  @Override
  public List<UserTags> getByCategory(String category) {
    LambdaQueryWrapper<UserTags> wrapper = new LambdaQueryWrapper<>();
    wrapper.like(UserTags::getCategory, category);

    return list(wrapper);
  }

  @Override
  public UserTags getByUid(int uid) {
    LambdaQueryWrapper<UserTags> wrapper = new LambdaQueryWrapper<>();
    wrapper.eq(UserTags::getUid, uid);

    return getOne(wrapper, false);
  }
}




