package com.asggo.g21.biz.mapper;

import com.asggo.g21.biz.domain.UserTags;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author LannLife
 */
public interface UserTagsMapper extends BaseMapper<UserTags> {

}




