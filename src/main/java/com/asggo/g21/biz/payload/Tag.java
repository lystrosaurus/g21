package com.asggo.g21.biz.payload;

import lombok.Data;

/**
 * 标签.
 *
 * @author eric 2024/4/28 13:59
 */
@Data
public class Tag {

  /**
   * 标签ID.
   */
  private Integer id;
  /**
   * 标签分类.
   */
  private String category;
  /**
   * 标签值.
   */
  private String value;
}
