package com.asggo.g21.biz.domain;

import com.asggo.g21.biz.payload.Tag;
import com.baomidou.mybatisplus.annotation.FieldStrategy;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.JacksonTypeHandler;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * 用户标签 demo
 */
@TableName(value = "user_tags")
@Data
public class UserTags implements Serializable {

  /**
   *
   */
  @TableId(type = IdType.AUTO)
  private Integer id;

  /**
   *
   */
  @TableField("uid")
  private Integer uid;

  /**
   *
   */
  @TableField(value = "tags", typeHandler = JacksonTypeHandler.class)
  private List<Tag> tags;

  @TableField(value = "tags ->> '$[*].category'",
      insertStrategy = FieldStrategy.NEVER,
      updateStrategy = FieldStrategy.NEVER,
      select = false)
  private String category;

  @Serial
  @TableField(exist = false)
  private static final long serialVersionUID = 1L;
}