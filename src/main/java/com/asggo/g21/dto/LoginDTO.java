package com.asggo.g21.dto;

import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 18:55
 */
@Data
public class LoginDTO {

  /**
   * 用户名.
   */
  private String username;
  /**
   * 密码.
   */
  private String password;
}
