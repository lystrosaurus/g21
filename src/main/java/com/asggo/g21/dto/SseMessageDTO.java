package com.asggo.g21.dto;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/3/27 17:46
 */
@Data
public class SseMessageDTO implements Serializable {

  @Serial
  private static final long serialVersionUID = 1L;

  /**
   * 需要推送到的session key 列表
   */
  private List<String> sessionKeys;

  /**
   * 需要发送的消息
   */
  private String message;
}
