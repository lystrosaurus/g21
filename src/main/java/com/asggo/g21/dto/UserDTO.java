package com.asggo.g21.dto;

import java.io.Serializable;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/4/16 9:43
 */
@Data
public class UserDTO implements Serializable {

  private Integer id;
  private String name;
  private String email;
  private String phone;
}
