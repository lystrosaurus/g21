package com.asggo.g21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class G21Application {

	public static void main(String[] args) {
		SpringApplication.run(G21Application.class, args);
	}

}
