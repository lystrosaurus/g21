package com.asggo.g21.stomp;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/15 14:05
 */
@Getter
public class Greeting {

  private String content;

  public Greeting() {
  }

  public Greeting(String content) {
    this.content = content;
  }

}
