package com.asggo.g21.enums;

import lombok.Getter;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/20 13:02
 */
@Getter
public enum ResultCode {
  RC_SUCCESS("00000", "一切 ok"),
  RC_FAIL("A0001", "用户端错误"),

  USER_REGISTRATION_ERROR("A0100", "用户注册错误"),
  LOGIN_FAIL("A0101", "登录失败"),
  NOT_LOGIN("A0102", "未登录"),
  NOT_ALLOWED_LOGIN("A0103", "您输入的手机号码不可登录,如需帮助,请联系工作人员"),
  NOT_ROLE("A0104", "当前账户没有该操作的权限"),
  USER_EXISTED("A0105", "用户已经注册"),

  SIGNATURE_ERROR("A0200", "签名错误"),
  METHOD_ARGUMENT_NOT_VALID_EXCEPTION("A0300", "参数校验失败"),
  RESUBMIT("A0400", "Please do not resubmit!"),

  ;


  private final String code;
  private final String message;

  ResultCode(String code, String message) {
    this.code = code;
    this.message = message;
  }
}
