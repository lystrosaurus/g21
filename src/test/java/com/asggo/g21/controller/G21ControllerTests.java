package com.asggo.g21.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import cn.hutool.http.HttpUtil;
import com.asggo.g21.dto.LoginDTO;
import com.asggo.g21.payload.fs.Message;
import com.asggo.g21.payload.fs.TextContent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/21 17:33
 */
class G21ControllerTests extends BaseControllerTests {

  @Test
  void virtualThread() throws Exception {
    mockMvc.perform(
            post("/g21/virtualThread")
                .header(tokenName, tokenValue)
        ).andDo(print())
        .andExpect(status().is2xxSuccessful());
  }

  @Test
  void fs() throws JsonProcessingException {
    LoginDTO loginDTO = new LoginDTO();
    loginDTO.setPassword("To make each day count.");
    loginDTO.setUsername(null);
    Message msg = new Message();
    TextContent content = new TextContent();
    content.setText(new ObjectMapper().writeValueAsString(loginDTO));
    msg.setContent(content);
    final String post = HttpUtil.post(
        "https://open.feishu.cn/open-apis/bot/v2/hook/8abdfb2b-da58-496c-846b-b63a40c75f9f",
        new ObjectMapper().writeValueAsString(msg));
    assertThat(post).isNotEmpty();
  }
}
