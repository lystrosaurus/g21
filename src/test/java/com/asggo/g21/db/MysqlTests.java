package com.asggo.g21.db;

import static org.assertj.core.api.Assertions.assertThat;

import cn.hutool.db.Db;
import cn.hutool.db.ds.simple.SimpleDataSource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import javax.sql.DataSource;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/1/3 18:28
 */
@SpringBootTest
class MysqlTests {

  @Test
  void conn() throws SQLException {
    DataSource ds = new SimpleDataSource("jdbc:mysql://localhost:3306/lann", "root", "root");

    final Db db = Db.use(ds);
    final Connection connection = db.getConnection();
    assertThat(connection).isNotNull();
  }

  @Autowired
  JdbcTemplate jdbcTemplate;

  @Test
  void jdbc() throws JsonProcessingException {
    final Map<String, Object> stringObjectMap = jdbcTemplate.queryForMap(
        "select * from `dept` limit 1");
    assertThat(stringObjectMap).isNotEmpty();

    System.out.println(new ObjectMapper().writeValueAsString(stringObjectMap));
    
  }
}
