package com.asggo.g21.config;

import java.nio.charset.Charset;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2023/12/21 16:46
 */
@Configuration
@AutoConfigureAfter({WebMvcAutoConfiguration.class})
public class MockConfig {

  @Bean
  MockMvcBuilderCustomizer responseCharacterEncodingCustomizer() {
    return builder -> builder.alwaysDo(result
        -> result.getResponse().setCharacterEncoding(Charset.defaultCharset().toString()));
  }
}
