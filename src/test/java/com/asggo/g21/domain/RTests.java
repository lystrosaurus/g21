package com.asggo.g21.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.asggo.g21.enums.ResultCode;
import java.io.Serializable;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/1/4 13:41
 */
@SpringBootTest
class RTests {

  final String msg = "To make each day count.";

  @Test
  void ok() {
    final R<Serializable> r = R.ok(msg);
    assertThat(r.body()).isEqualTo(msg);
  }

  @Test
  void fail() {
    assertThat(R.fail().body()).isNull();
  }

  @Test
  void fail2() {
    assertThat(R.fail(msg).body()).isEqualTo(msg);
  }

  @Test
  void fail3() {
    assertThat(
        R.fail(ResultCode.LOGIN_FAIL.getCode(), ResultCode.LOGIN_FAIL.getMessage())
            .body()
    ).isNull();
  }

  @Test
  void isError() {
    assertThat(R.isError(R.ok())).isFalse();
  }
}
