package com.asggo.g21.biz.dao;

import static org.assertj.core.api.Assertions.assertThat;

import cn.hutool.core.util.RandomUtil;
import com.apifan.common.random.RandomSource;
import com.apifan.common.random.entity.CountryOrRegionCode;
import com.apifan.common.random.entity.EconomicCategory;
import com.asggo.g21.biz.domain.UserTags;
import com.asggo.g21.biz.payload.Tag;
import com.google.common.collect.ImmutableList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/4/28 14:09
 */
@SpringBootTest
class UserTagsDaoTests {

  @Autowired
  UserTagsDao userTagsDao;

  @Test
  void save() {
    final UserTags userTags = randomUserTags();

    userTagsDao.save(userTags);

    assertThat(userTags.getId()).isNotNull();
  }

  @Test
  void getByCategory() {
    List<UserTags> userTags = userTagsDao.getByCategory("香港");
    assertThat(userTags).isNotEmpty();
  }

  @Test
  void getByUid() {
    UserTags userTags = userTagsDao.getByUid(911);
    assertThat(userTags).isNotNull();
  }

  private UserTags randomUserTags() {
    final UserTags userTags = new UserTags();
    userTags.setUid(RandomUtil.randomInt(1, 1000));

    // 国家
    final CountryOrRegionCode countryOrRegionCode = RandomSource.areaSource()
        .randomCountryOrRegionCode();
    Tag tag1 = new Tag();
    tag1.setCategory(countryOrRegionCode.getNameCN());
    tag1.setValue(countryOrRegionCode.getNumber());
    tag1.setId(1);

    // 行业分类
    final EconomicCategory economicCategory = RandomSource.otherSource().randomEconomicCategory();
    Tag tag2 = new Tag();
    tag2.setCategory(economicCategory.getName());
    tag2.setValue(economicCategory.getCode());
    tag2.setId(2);
    userTags.setTags(ImmutableList.of(tag1, tag2));

    return userTags;
  }
}
