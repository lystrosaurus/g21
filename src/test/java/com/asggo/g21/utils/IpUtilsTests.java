package com.asggo.g21.utils;

import static org.assertj.core.api.Assertions.assertThat;

import cn.hutool.core.net.Ipv4Util;
import org.junit.jupiter.api.Test;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/2/27 11:29
 */

class IpUtilsTests {

  @Test
  void getRegion() {
    // 218.250.217.163
    // final String ip = "180.124.68.28";
    final String ip = "124.79.127.253";
    final boolean innerIP = Ipv4Util.isInnerIP("10.0.3.14");
    assertThat(innerIP).isTrue();
    final String region = IpUtils.getRegion(ip);
    System.out.println(region);
    assertThat(region).isNotNull();
  }

}
