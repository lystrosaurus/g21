package com.asggo.g21.utils;

import com.lark.oapi.Client;
import com.lark.oapi.core.request.RequestOptions;
import com.lark.oapi.core.utils.Jsons;
import com.lark.oapi.service.bitable.v1.model.AppTableRecord;
import com.lark.oapi.service.bitable.v1.model.CreateAppTableRecordReq;
import com.lark.oapi.service.bitable.v1.model.CreateAppTableRecordResp;
import com.lark.oapi.service.bitable.v1.model.SearchAppTableRecordReq;
import com.lark.oapi.service.bitable.v1.model.SearchAppTableRecordReqBody;
import com.lark.oapi.service.bitable.v1.model.SearchAppTableRecordResp;
import com.lark.oapi.service.bitable.v1.model.Sort;
import com.lark.oapi.service.drive.v1.model.UploadAllFileReq;
import com.lark.oapi.service.drive.v1.model.UploadAllFileReqBody;
import com.lark.oapi.service.drive.v1.model.UploadAllFileResp;
import com.lark.oapi.service.drive.v1.model.UploadAllMediaReq;
import com.lark.oapi.service.drive.v1.model.UploadAllMediaReqBody;
import com.lark.oapi.service.drive.v1.model.UploadAllMediaResp;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/5/21 10:01
 */
public class OpenApiTests {

  public static void main(String[] args) throws Exception {
    // records();
    // uploadAllMedia();
    searchAppTableRecord();
  }

  public static void searchAppTableRecord() throws Exception {
    Client client = Client.newBuilder("cli_a57bd22e4338900c", "2FpAhx8p1pLevkEzL96yEbThxjzP0QwK")
        .build();

    // 创建请求对象
    SearchAppTableRecordReq req = SearchAppTableRecordReq.newBuilder()
        .appToken("ALaXb0uU4abkTPsNn3AcrdT0nQ8")
        .tableId("tblOj5s20AIFQXtu")
        .pageSize(6)
        .pageToken("cGFnZVRva2VuOjY=")
        .searchAppTableRecordReqBody(SearchAppTableRecordReqBody.newBuilder()
            .viewId("vewroow3Kh")
            .fieldNames(new String[]{
                "反馈单id",
                "订单id",
                "联系方式",
                "用户昵称",
                "创建时间",
                "反馈对象",
                "问题类型",
                "用户描述",
                "处理人",
                "处理状态",
                "处理结果"
            })
            .sort(new Sort[]{
                Sort.newBuilder()
                    .fieldName("创建时间")
                    .desc(true)
                    .build()
            })
//            .filter(FilterInfo.newBuilder()
//                .conjunction("and")
//                .conditions(new Condition[]{
//                    Condition.newBuilder()
//                        .fieldName("联系方式")
//                        .operator("is")
//                        .value(new String[]{
//                            "13912345678"
//                        })
//                        .build()
//                })
//                .build())
            .automaticFields(false)
            .build())
        .build();

    // 发起请求
    SearchAppTableRecordResp resp = client.bitable().appTableRecord()
        .search(req, RequestOptions.newBuilder()
            .build());

    // 处理服务端错误
    if (!resp.success()) {
      System.out.printf("code:%s,msg:%s,reqId:%s%n", resp.getCode(), resp.getMsg(),
          resp.getRequestId());
      return;
    }

//    final SearchAppTableRecordResp searchAppTableRecordResp = JSONUtil.toBean(
//        new String(resp.getRawResponse().getBody(), StandardCharsets.UTF_8),
//        SearchAppTableRecordResp.class);
//    System.out.println(Jsons.DEFAULT.toJson(searchAppTableRecordResp));

    // 业务数据处理
    System.out.println(Jsons.DEFAULT.toJson(resp.getData()));
  }

  public static void uploadAllMedia() throws Exception {
    // OpujbmeaxoTVW5xIA0eceovonAg
    // 构建client
    Client client = Client.newBuilder("cli_a57bd22e4338900c", "2FpAhx8p1pLevkEzL96yEbThxjzP0QwK")
        .build();

    File file = new File("C:\\Users\\LannLife\\Pictures\\Saved Pictures\\bridge-53769.jpg");

    // 创建请求对象
    UploadAllMediaReq req = UploadAllMediaReq.newBuilder()
        .uploadAllMediaReqBody(UploadAllMediaReqBody.newBuilder()
            .fileName(file.getName())
            .parentType("bitable_image")
            .parentNode("Gpb6bVUD7acjxyslaO2c0eONnab")
            .size((int) file.length())
            .checksum("")
            .extra("")
            .file(file)
            .build())
        .build();

    // 发起请求
    UploadAllMediaResp resp = client.drive().media().uploadAll(req, RequestOptions.newBuilder()
        .build());

    // 处理服务端错误
    if (!resp.success()) {
      System.out.printf("code:%s,msg:%s,reqId:%s%n", resp.getCode(), resp.getMsg(),
          resp.getRequestId());
      return;
    }

    // 业务数据处理
    System.out.println(Jsons.DEFAULT.toJson(resp.getData()));
  }

  public static void records() throws Exception {
    // 构建client
    Client client = Client.newBuilder("cli_a57bd22e4338900c", "2FpAhx8p1pLevkEzL96yEbThxjzP0QwK")
        .build();

    // 创建请求对象
    CreateAppTableRecordReq req = CreateAppTableRecordReq.newBuilder()
        .appToken("Gpb6bVUD7acjxyslaO2c0eONnab")
        .tableId("tblkKp21nq1HDdDc")
        .appTableRecord(AppTableRecord.newBuilder()
            .fields(new HashMap<>() {
              {
                put("项目名称", "吐槽");
                put("项目情况概述", "吐槽项目情况概述");
                put("总负责人", List.of(Map.of("id", "ou_b47c179e318c7569bd294cc6a8a3849a")));
                put("图片测试", List.of(Map.of("file_token", "Vwvlb0Vh7oBKLGx6KghcfSQMnZf")));
                put("状态", "已完成");
              }
            })
            .build())
        .build();

    // 发起请求
    CreateAppTableRecordResp resp = client.bitable().appTableRecord().create(req);

    // 处理服务端错误
    if (!resp.success()) {
      System.out.printf("code:%s,msg:%s,reqId:%s%n", resp.getCode(), resp.getMsg(),
          resp.getRequestId());
      return;
    }

    // 业务数据处理
    System.out.println(Jsons.DEFAULT.toJson(resp.getData()));
  }

  public static String uploadFile() throws Exception {
    // 构建client
    Client client = Client.newBuilder("cli_a57bd22e4338900c", "2FpAhx8p1pLevkEzL96yEbThxjzP0QwK")
        .build();

    File file = new File("C:\\Users\\LannLife\\Pictures\\Saved Pictures\\bridge-53769.jpg");

    // 创建请求对象
    UploadAllFileReq req = UploadAllFileReq.newBuilder()
        .uploadAllFileReqBody(UploadAllFileReqBody.newBuilder()
            .fileName(file.getName())
            .parentType("explorer")
            .parentNode("JWoBfdNHLlyf2NdLuHZcj5w5n8S")
            .size((int) file.length())
            .file(file)
            .build())
        .build();

    // 发起请求
    UploadAllFileResp resp = client.drive().file().uploadAll(req, RequestOptions.newBuilder()
        .build());

    // 处理服务端错误
    if (!resp.success()) {
      System.out.printf("code:%s,msg:%s,reqId:%s%n", resp.getCode(), resp.getMsg(),
          resp.getRequestId());
      return resp.getMsg();
    }

    // 业务数据处理
    System.out.println(Jsons.DEFAULT.toJson(resp.getData()));
    // {"file_token":"CQwWbcpm9o0sssxCkdVchZucnXi"}
    return resp.getData().getFileToken();
  }
}
