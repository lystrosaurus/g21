package com.asggo.g21.utils;

import static org.assertj.core.api.Assertions.assertThat;

import com.asggo.g21.dto.SseMessageDTO;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Created by IntelliJ IDEA.
 *
 * @author eric 2024/3/27 14:05
 */
@SpringBootTest
class SseEmitterUtilsTests {

  @Test
  void publishMessage() {
    SseMessageDTO message = new SseMessageDTO();
    message.setSessionKeys(List.of("1001"));
    message.setMessage("1001 您好");

    SseEmitterUtil.publishMessage(message);

    assertThat(this).isNotNull();
  }
}
